/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.additionaltask1;
import java.util.Scanner;
/**
 *
 * @author spoty
 */
public class AdditionalTask1 {

    public static void main(String[] args) {
     Scanner scanner = new Scanner(System.in); 
     System.out.println("Write x");
     float x = scanner.nextFloat();
     System.out.println("Write y");
     float y = scanner.nextFloat();
     float z1 = x+y;
     float z2 = x-y;
     float z3 = x*y;
     float z4 = x/y;
     System.out.println("x+y="+z1);
     System.out.println("x-y="+z2);
     System.out.println("x*y="+z3);
     System.out.println("x/y="+z4);
     
    }
}
